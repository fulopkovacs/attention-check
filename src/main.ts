import { createApp } from "vue";
import { createI18n } from "vue-i18n";
import messages from "./messages";
import App from "./App.vue";
import "./index.sass";

function setLocale(l: string) {
  if (/^hu\b/.test(l)) {
    return "hu";
  } else if (/^en\b/.test(l)) {
    return "en";
  }
}

const i18n = createI18n({
  locale: setLocale(navigator.language),
  fallbackLocale: "en",
  messages,
});

const app = createApp(App);

app.use(i18n);
app.mount("#app");
