const messages = {
  en: {
    timeDesc: "Alert me in every",
    timeUnit: "minutes",
    btn_START: "start",
    btn_PAUSE: "pause",
    btn_RESUME: "resume",
    btn_RESET: "reset",
    footer_desc: "Designed and developed by: Fülöp Kovács",
    footer_sound_credits: "Sound credits",
    credits: {
      close: "close",
      title: "Sound Credits",
      link_label: "Click here for the original work.",
      do_it:
        "Excerpt from #INTRODUCTIONS by Labeour, Ronkko & Turner. Released under Creative Commons license.",
      dong:
        "The author of this sound effect is Aiwha. Released under Creative Commons license.",
    },
  },
  hu: {
    timeDesc: "Figyelmeztess",
    timeUnit: "percenként",
    btn_START: "start",
    btn_PAUSE: "szünet",
    btn_RESUME: "folytatás",
    btn_RESET: "stop",
    footer_desc: "Design és fejlesztés: Kovács Fülöp",
    footer_sound_credits: "A hangeffektusok forrásai",
    credits: {
      close: "bezár",
      title: "A hangeffektusok forrásai",
      link_label: "Itt található az eredeti munka.",
      do_it:
        "Részlet Labeour, Ronkko & Turner #INTRODUCTIONS című művéből. Licensz: Creative Commons.",
      dong: "A hangeffekt szerzője Aiwha. Licensz: Creative Commons.",
    },
  },
};

export default messages;
